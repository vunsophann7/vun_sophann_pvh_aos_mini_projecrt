package com.example.photoexhibitionapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.photoexhibitionapp.Data.GalleryAlbums;
import com.example.photoexhibitionapp.GalleryActivity;
import com.example.photoexhibitionapp.R;
import com.example.photoexhibitionapp.databinding.ActivityAlbumnGalleryBinding;
import com.example.photoexhibitionapp.databinding.ActivityGalleryBinding;

import java.util.ArrayList;
import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder> {

    private final Context context;
    private final List<String> stringListImage;
    private final List<String> stringListTitle;
    private final List<String> stringListViews;

    public GalleryAdapter(Context context, List<String> stringListImage, List<String> stringListTitle, List<String> stringListViews) {
        this.context = context;
        this.stringListImage = stringListImage;
        this.stringListTitle = stringListTitle;
        this.stringListViews = stringListViews;
    }

    @NonNull
    @Override
    public GalleryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new GalleryViewHolder(LayoutInflater.from(context).inflate(R.layout.activity_albumn_gallery, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryViewHolder holder, int position) {
        holder.txtGalleryTitle.setText(stringListTitle.get(position));
        holder.txtGalleryView.setText(stringListViews.get(position));
//        holder.imageView.setImageResource(items.get(position).getIv());
        Glide.with(context)
                .load(stringListImage.get(position))
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return stringListImage.size();
    }

    public class GalleryViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView txtGalleryView, txtGalleryTitle;
        ActivityAlbumnGalleryBinding binding;

        public GalleryViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ActivityAlbumnGalleryBinding.bind(itemView);
            imageView = binding.imageGalleryAlbums;
            txtGalleryView = binding.txtGalleryView;
            txtGalleryTitle = binding.txtGalleryTitle;
//            binding = ActivityAlbumnGalleryBinding.inflate(LayoutInflater.from(itemView.getContext()));
//            imageView = itemView.findViewById(R.id.imageGalleryAlbums);
//            txtGalleryView = itemView.findViewById(R.id.txtGalleryView);
//            txtGalleryTitle = itemView.findViewById(R.id.txtGalleryTitle);
        }
    }
}
