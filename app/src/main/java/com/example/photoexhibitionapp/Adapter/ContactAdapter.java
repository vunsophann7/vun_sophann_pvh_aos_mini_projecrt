package com.example.photoexhibitionapp.Adapter;

import static androidx.core.content.ContextCompat.startActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.photoexhibitionapp.ContactActivity;
import com.example.photoexhibitionapp.Data.ContactData;
import com.example.photoexhibitionapp.Data.OnItemClickListener;
import com.example.photoexhibitionapp.R;
import com.example.photoexhibitionapp.databinding.ActivityAlbumnGalleryBinding;
import com.example.photoexhibitionapp.databinding.ActivityContactBinding;
import com.example.photoexhibitionapp.databinding.ActivityContactNumberCardBinding;

import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder>{
    private Context context;
    private ArrayList<String> stringListImage;
    private ArrayList<String> stringListName;
    private ArrayList<String> stringsListNumber;
    ActivityContactNumberCardBinding binding;
    private final OnItemClickListener listener;
    public ContactAdapter(Context context, ArrayList<String> stringListImage, ArrayList<String> stringListName, ArrayList<String> stringsListNumber,OnItemClickListener listener) {
        this.context = context;
        this.stringListImage = stringListImage;
        this.stringListName = stringListName;
        this.stringsListNumber = stringsListNumber;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        return new ContactViewHolder(LayoutInflater.from(context).inflate(R.layout.activity_contact_number_card, parent, false));
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_contact_number_card,parent,false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.txtUerName.setText(stringListName.get(position));
        Glide.with(context)
                .load(stringListImage.get(position))
                .into(holder.imageView);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(stringsListNumber.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return stringListName.size();
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder {


        ImageView imageView;
        TextView txtUerName;
        ConstraintLayout cardView;

        public ContactViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ActivityContactNumberCardBinding.bind(itemView);
            imageView = binding.ivProfile;
            txtUerName = binding.txtUserName;
            cardView = binding.intentToPhoneCall;
//            binding = ActivityContactNumberCardBinding.inflate(LayoutInflater.from(itemView.getContext()));
//            imageView=binding.ivProfile;
//            txtUerName=binding.txtUserName;
//            imageView = itemView.findViewById(R.id.ivProfile);
//            txtUerName = itemView.findViewById(R.id.txtUserName);
        }



    }
}
