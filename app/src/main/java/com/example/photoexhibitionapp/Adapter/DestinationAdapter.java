package com.example.photoexhibitionapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.photoexhibitionapp.Data.DestinationImage;
import com.example.photoexhibitionapp.R;
import com.example.photoexhibitionapp.databinding.ActivityDestinationBinding;

import java.util.ArrayList;

public class DestinationAdapter extends RecyclerView.Adapter<DestinationAdapter.DestinationViewHolder>{
    private final Context context;
    private final ArrayList<String> arrayList;

    public DestinationAdapter(Context context, ArrayList<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public DestinationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_destination,parent,false);
        return new DestinationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DestinationViewHolder holder, int position) {
//            holder.binding.imageDestination.setImageResource(arrayList.get(position).getImageDestination());
        Glide.with(context)
                .load(arrayList.get(position))
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class DestinationViewHolder extends RecyclerView.ViewHolder {
        ActivityDestinationBinding binding;
        ImageView imageView;
        public DestinationViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ActivityDestinationBinding.bind(itemView);
            imageView = binding.imageDestination;
//            binding = ActivityDestinationBinding.inflate(LayoutInflater.from(itemView.getContext()));
//            imageView = itemView.findViewById(R.id.imageDestination);
        }
    }
}
