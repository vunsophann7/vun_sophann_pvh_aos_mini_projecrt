package com.example.photoexhibitionapp.Data;

public class ContactData {
    private String userImage;
    private String userName;
    private String phoneNumber;

    public ContactData(String userImage, String userName, String phoneNumber) {
        this.userImage = userImage;
        this.userName = userName;
        this.phoneNumber = phoneNumber;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
