package com.example.photoexhibitionapp.Data;

public class DestinationImage {
    private String imageDestination;

    public DestinationImage(String imageDestination) {
        this.imageDestination = imageDestination;
    }

    public String getImageDestination() {
        return imageDestination;
    }

    public void setImageDestination(String imageDestination) {
        this.imageDestination = imageDestination;
    }
}
