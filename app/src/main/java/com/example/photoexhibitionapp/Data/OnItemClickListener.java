package com.example.photoexhibitionapp.Data;

public interface OnItemClickListener {
    void onItemClick(String item);
}
