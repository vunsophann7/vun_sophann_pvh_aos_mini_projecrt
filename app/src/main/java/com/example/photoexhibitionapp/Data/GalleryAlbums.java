package com.example.photoexhibitionapp.Data;

public class GalleryAlbums {

    private String txtGalleryTitle;
    private String txtGalleryView;
    private String iv;

    public GalleryAlbums(String txtGalleryTitle, String txtGalleryView, String iv) {
        this.txtGalleryTitle = txtGalleryTitle;
        this.txtGalleryView = txtGalleryView;
        this.iv = iv;
    }

    public String getTxtGalleryTitle() {
        return txtGalleryTitle;
    }

    public void setTxtGalleryTitle(String txtGalleryTitle) {
        this.txtGalleryTitle = txtGalleryTitle;
    }

    public String getTxtGalleryView() {
        return txtGalleryView;
    }

    public void setTxtGalleryView(String txtGalleryView) {
        this.txtGalleryView = txtGalleryView;
    }

    public String getIv() {
        return iv;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }
}
