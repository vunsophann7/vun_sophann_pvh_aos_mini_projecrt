package com.example.photoexhibitionapp.Data;

public class User {
    private String name;
    private String career;
    private String gender;

    public User(String name, String career, String gender) {
        this.name = name;
        this.career = career;
        this.gender = gender;
    }

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
