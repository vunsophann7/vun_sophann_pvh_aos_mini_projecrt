package com.example.photoexhibitionapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.photoexhibitionapp.Adapter.ContactAdapter;
import com.example.photoexhibitionapp.Data.OnItemClickListener;
import com.example.photoexhibitionapp.databinding.ActivityContactBinding;
import com.example.photoexhibitionapp.databinding.ActivityContactNumberCardBinding;

import java.util.ArrayList;
import java.util.List;

public class ContactActivity extends AppCompatActivity {

    ActivityContactBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(ContactActivity.this, R.layout.activity_contact);

        ArrayList<String> stringListImage = new ArrayList<>();
        ArrayList<String> stringListUserName = new ArrayList<>();
        ArrayList<String> stringListNumber = new ArrayList<>();
        stringListImage.add("https://images.pexels.com/photos/1391498/pexels-photo-1391498.jpeg?auto=compress&cs=tinysrgb&w=600");
        stringListUserName.add("Lika");
        stringListNumber.add("0963452373");
        stringListImage.add("https://images.pexels.com/photos/775358/pexels-photo-775358.jpeg?auto=compress&cs=tinysrgb&w=600");
        stringListUserName.add("sofy");
        stringListNumber.add("0963465782");
        stringListImage.add("https://images.pexels.com/photos/1656684/pexels-photo-1656684.jpeg?auto=compress&cs=tinysrgb&w=600");
        stringListUserName.add("luiz");
        stringListNumber.add("0967890567");
        binding.recyclerContactOption.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        binding.recyclerContactOption.setAdapter(new ContactAdapter(getApplicationContext(), stringListImage, stringListUserName, stringListNumber, new OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
//                Toast.makeText(ContactActivity.this, ""+ item, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+item));
                startActivity(intent);
            }
        }));

    }
}