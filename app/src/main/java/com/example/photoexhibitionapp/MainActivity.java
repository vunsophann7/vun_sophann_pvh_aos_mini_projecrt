package com.example.photoexhibitionapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.example.photoexhibitionapp.Adapter.DestinationAdapter;
import com.example.photoexhibitionapp.Data.DestinationImage;
import com.example.photoexhibitionapp.databinding.ActivityMainBinding;
import com.example.photoexhibitionapp.databinding.NotebookDialogBinding;
import com.google.android.material.chip.Chip;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    Intent intent;
    String[] musicAlbums = {"Jazz","Hip hop music","Pop music","Rock music","Classical music"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(MainActivity.this, R.layout.activity_main);
        ArrayList<String> items = new ArrayList<>();
        items.add("https://images.pexels.com/photos/2662116/pexels-photo-2662116.jpeg?auto=compress&cs=tinysrgb&w=600");
        items.add("https://images.pexels.com/photos/1324803/pexels-photo-1324803.jpeg?auto=compress&cs=tinysrgb&w=600");
        items.add("https://images.pexels.com/photos/1271619/pexels-photo-1271619.jpeg?auto=compress&cs=tinysrgb&w=600");
        binding.recyclerDestination.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.recyclerDestination.setAdapter(new DestinationAdapter(getApplicationContext(), items));


        for (String music : musicAlbums) {
            Chip chip = new Chip(this);
            chip.setText(music);
            binding.chipGroup.addView(chip);
        }


        binding.imageOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity.this, GalleryActivity.class);
                startActivity(intent);
            }
        });

        binding.ContactOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity.this, ContactActivity.class);
                startActivity(intent);
            }
        });

        binding.btnNoteBookOption.setOnClickListener(view -> {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);

            NotebookDialogBinding binding1 = NotebookDialogBinding.inflate(LayoutInflater.from(this));
            builder.setTitle("Notebook Service")
                    .setView(binding1.getRoot());
            AlertDialog dialog = builder.create();
            dialog.show();

        });


        binding.btnAddUserOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity.this, AddUserActivity.class);
                startActivity(intent);
            }
        });

    }

}