package com.example.photoexhibitionapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.photoexhibitionapp.Data.User;
import com.example.photoexhibitionapp.databinding.ActivityAddUserBinding;

public class AddUserActivity extends AppCompatActivity {
    ActivityAddUserBinding binding;
    String gender = "";
    String btnRadioMale = "";
    String btnRadioFeMale = "";
    String name = "";
    String career = "";
    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(AddUserActivity.this, R.layout.activity_add_user);
        binding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnRadioMale = binding.radioMale.getText().toString();
                btnRadioFeMale = binding.radioFemale.getText().toString();
                name = binding.editName.getText().toString();
                career = binding.editCareer.getText().toString();
                int selectedId = binding.radioGroupButton.getCheckedRadioButtonId();
                if (selectedId == -1) {
                    // No radio button is selected, show an error message
                    Toast.makeText(getApplicationContext(), "Please select a gender", Toast.LENGTH_SHORT).show();
                }
                if (name.equals("")) {
                    binding.editName.setError("Empty field");
                } else if (career.equals("")) {
                    binding.editCareer.setError("Empty filed");
                } else {
                    btnRadioMale = binding.radioMale.getText().toString();
                    btnRadioFeMale = binding.radioFemale.getText().toString();
                    name = binding.editName.getText().toString();
                    career = binding.editCareer.getText().toString();

                    user = new User(name, career, gender);
                    binding.gender.setText(user.getGender());
                    binding.txtName.setText(user.getName());
                    binding.txtCareer.setText(user.getCareer());
                }
            }
        });

        binding.radioGroupButton.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checked) {
                if (checked == binding.radioMale.getId()) {
                    gender = "Mr";
                } else {
                    gender = "Mrs";
                }
            }
        });
    }

}