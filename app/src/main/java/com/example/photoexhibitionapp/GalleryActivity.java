package com.example.photoexhibitionapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;

import com.example.photoexhibitionapp.Adapter.GalleryAdapter;
import com.example.photoexhibitionapp.Data.GalleryAlbums;
import com.example.photoexhibitionapp.databinding.ActivityGalleryBinding;

import java.util.ArrayList;
import java.util.List;

public class GalleryActivity extends AppCompatActivity {
    ActivityGalleryBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(GalleryActivity.this, R.layout.activity_gallery);
        List<String> stringListImage = new ArrayList<>();
        List<String> stringListTitle = new ArrayList<>();
        List<String> stringListViews = new ArrayList<>();
        stringListImage.add("https://images.pexels.com/photos/147411/italy-mountains-dawn-daybreak-147411.jpeg?auto=compress&cs=tinysrgb&w=600");
        stringListTitle.add("Nature");
        stringListViews.add("1M views");
        stringListImage.add("https://images.pexels.com/photos/325807/pexels-photo-325807.jpeg?auto=compress&cs=tinysrgb&w=600");
        stringListTitle.add("Mountain");
        stringListViews.add("5M views");
        stringListImage.add("https://images.pexels.com/photos/1553963/pexels-photo-1553963.jpeg?auto=compress&cs=tinysrgb&w=600");
        stringListTitle.add("Ice Mountain");
        stringListViews.add("10M views");
        stringListImage.add("https://images.pexels.com/photos/1796727/pexels-photo-1796727.jpeg?auto=compress&cs=tinysrgb&w=600");
        stringListTitle.add("Landscape");
        stringListViews.add("7M views");
        stringListImage.add("https://images.pexels.com/photos/1271619/pexels-photo-1271619.jpeg?auto=compress&cs=tinysrgb&w=600");
        stringListTitle.add("Camping");
        stringListViews.add("25M views");
        binding.recyclerGalleryAlbums.setLayoutManager(new GridLayoutManager(this,2));
        binding.recyclerGalleryAlbums.setAdapter(new GalleryAdapter(getApplicationContext(), stringListImage, stringListTitle, stringListViews));
    }
}